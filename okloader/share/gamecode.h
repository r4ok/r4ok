/*
 * gamecode.h
 */

#ifndef __GAMECODE_H__
#define __GAMECODE_H__

#include <string.h>

inline unsigned int
game_code (const char *game_code_in)
{
  unsigned int game_code_out;
  memcpy (&game_code_out, game_code_in, 4);
  return game_code_out;
}

#endif
