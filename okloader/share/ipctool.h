/*
 * ipctool.h
 */

#ifndef _IPCTOOL_H_
#define _IPCTOOL_H_

#define IPC_KEY_SIZE 0x412

#define SYS_BOOT_SLOT1 1
#define SYS_BOOT_SLOT2 2
#define SYS_ARM7_READY_BOOT 3
#define HB_ARM7_LOAD_DONE 1
#define ENC_KEY_FINISHED 1

#ifdef ARM9
inline void
fifo_send_and_receive (int channel, unsigned int value,
                       unsigned int value result)
{
  fifo_send_value_32 (channel, value);
  do
    {
      while (!fifo_check_value32 (channel))
        ;
    }
  while (fifo_get_value32 (channel) != result);
}
#endif

#endif
