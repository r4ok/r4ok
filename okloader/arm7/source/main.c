/*
 * main.c
 */

#include "../../share/ipctool.h"
#include <nds.h>
#include <nds/arm7/clock.h>
#include <nds/system.h>
#include <stdint.h>
#include <string.h>

/*
 * arm9 cannot access memory space 0x37f8000 - 0x380ffff, so we
 * load arm7 code which can not be loaded by arm9 here
 * NOTE: this function can not load official roms because their arm7 code is
 * too big
 */

/* function prototypes */
void arm7_read_bios (void *dest, uint32_t src, uint32_t size); // read_bios.s
void arm7_swi_soft_reset (void);                               // soft_reset.s
void
vblank_handler (void)
{
}
void
__libnds_exit (int rc)
{
}

/* static utility functions */
static void
arm7_load_homebrew (uint32_t a_size)
{
  static uint32_t homebrew_offset = 0;
  memcpy ((void *)(__NDSHeader->arm7destination + homebrew_offset),
          (void *)0x23fc000, a_size);
  homebrew_offset += a_size;
  (FIFO_USER_02, HB_ARM7_LOAD_DONE);
}

static void
arm7_get_key (void *a_buf)
{
  arm7_read_bios (a_buf, 0x30, IPC_KEY_SIZE * 4);
  fifo_send_value_32 (FIFO_USER_03, ENC_KEY_FINISHED);
}

static void
arm7_prepare_reset ()
{
  REG_IME = IME_DISABLE;
  REG_POWERCNT |= POWER_SOUND;

  /* reset DMA and timers */
  for (uint32_t i = 0; i < 4; ++i)
    {
      DMA_SRC (i) = 0;
      DMA_DEST (i) = 0;
      DMA_CR (i) = 0;
      TIMER_CR (i) = 0;
      TIMER_DATA (i) = 0;
    }

  REG_IE = 0;
  REG_IF = -1;
  REG_IPC_SYNC = 0;

  fifo_send_value_32 (FIFO_USER_01, SYS_ARM7_READY_BOOT);
}

static uint32_t
generate_armb (uint32_t aSource, uint32_t aTarget)
{
  uint32_t result = 0;
  int diff = aTarget - aSource - 8;

  if (diff <= 33554428 && diff >= -33554432 && (diff & 3) == 0)
    {
      result = 0xea000000 | ((diff >> 2) & 0xffffff);
    }

  return result;
}

/* message handlers */
void
arm7_msghandler_system (uint32_t value, void *data)
{
  if (value == SYS_BOOT_SLOT2)
    {
      arm7_prepare_reset ();
      arm7_swi_soft_reset ();
    }
}

void
arm7_msghandler_homebrew (uint32_t value, void *data)
{
  arm7_load_homebrew (value);
}

void
arm7_msghandler_encryption (uint32_t value, void *data)
{
  arm7_get_key ((void *)value);
}

void
arm7_msghandler_reset (uint32_t value, void *data)
{
  uint32_t *i, *isr = 0;

  for (i = (uint32_t *)__NDSHeader->arm7destination;
       i < (uint32_t *)(__NDSHeader->arm7destination
                        + __NDSHeader->arm7binarySize);
       i++)
    {
      /* 00 00 4F E1   mrs r0, spsr */
      if (*i == 0xe14f0000)
        {
          isr = i;
          break;
        }
    }

  if (isr != NULL)
    {
      *isr = generate_armb ((uint32_t)isr, value);
    }

  fifo_send_value_32 (FIFO_USER_04, (uint32_t)isr);
}

/* arm7 entry point */
int
main (int argc, char **argv)
{
  /* initialize IRQ and setup vblank handler */
  irq_init ();
  irq_set (IRQ_VBLANK, vblank_handler);
  irq_enable (IRQ_VBLANK);

  /* initizlize FIFO and set value handlers */
  fifo_init ();
  fifo_set_value32_handler (FIFO_USER_01, arm7_msghandler_system, 0);
  fifo_set_value32_handler (FIFO_USER_02, arm7_msghandler_homebrew, 0);
  fifo_set_value32_handler (FIFO_USER_03, arm7_msghandler_encryption, 0);
  fifo_set_value32_handler (FIFO_USER_04, arm7_msghandler_reset, 0);

  /* Prevent the ARM7 from accessing main RAM */
  while (1)
    arm7_swi_wait_vblank ();
}
